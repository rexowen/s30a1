

/*Business Logic for POST /signup
1. Add a functionality to check if there are duplicate tasks
-If the user already exists in the database, we return an error
-If the user doesn't exist in the database, we add it in the database
2. The user data will be coming from the request's body
3. Create a new user object with a "username" and "password" fields/properties*/

/*Business Logic for GET /users
1. We will retrive all the documents using the get method
2. If an error is encountered, print the error
3. If no errors are found, send a success status back to the client/postman and return an array of document*/

const express = require('express');;
const mongoose = require('mongoose');
const app = express();
const port = 3001;

mongoose.connect("mongodb+srv://admin:PassWord1@zuitt-bootcamp.qr3zf.mongodb.net/batch_127to-do?retryWrites=true&w=majority",
		{
			useNewUrlParser: true,
			useUnifiedTopology: true,
		}
);
let db = mongoose.connection;
db.on("error", console.error.bind(console, "connection error"))
db.once("open", () => console.log("We're connected to the cloud database"));

app.use(express.json());
app.use(express.urlencoded({ extended:true }));

const userSchema = new mongoose.Schema({

	name: String,
	password: String,
	status: {
		type: String,

		default: "pending"
	}
})

const User = mongoose.model("User", userSchema)

app.post('/signup', (req, res) => {
	User.findOne({ name: req.body.name }, (err, result) => {
	if(result !== null && result.name == req.body.name ){
		return res.send("User Duplicate Found")
	}else {
		let newUser = new User ({
			name: req.body.name,
			password: req.body.password
		})

		newUser.save((saveErr, savedUser) => {
			if(saveErr){
				return console.error(saveErr)
			}else {
				return res.status(201).send("New user registered");
			}
		})
	}
	})
})

app.get('/users', (req, res) => {
	User.find( {}, (err, result) => {
		if(err){
			return console.log(err)
		}else {
			return res.status(200).json({
				data: result
			})
		}
	})
})

app.listen(port, () => console.log(`Server is running at port ${port}`));;